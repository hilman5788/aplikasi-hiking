import Product from "../../models/about";

export const DELETE_ABOUT = "DELETE_ABOUT";
export const CREATE_ABOUT = "CREATE_ABOUT";
export const UPDATE_ABOUT = "UPDATE_ABOUT";
export const SET_ABOUT = "SET_ABOUT";

export const fethAbout = () => {
  return async (dispatch, getState) => {
    // any async code you want!
    const userId = getState().auth.userId;
    try {
      const response = await fetch(
        // "https://react-sample-9a61e.firebaseio.com/products.json"
        "https://react-sample-9a61e.firebaseio.com/about.json"
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const resData = await response.json();
      // console.log('ini about',resData);
      // return;
      const loadedProducts = [];

      for (const key in resData) {
        loadedProducts.push(
          new Product(
            key,
            resData[key].ownerId,
            resData[key].title,
            resData[key].imageUrl,
            resData[key].description,
            resData[key].price
          )
        );
      }

      dispatch({
        type: SET_ABOUT,
        products: loadedProducts,
        userProducts: loadedProducts.filter((prod) => prod.ownerId === userId),
      });
    } catch (err) {
      // send to custom analytics server
      throw err;
    }
  };
};


export const deleteAbout = (productId) => {
  return async (dispatch, getState) => {
    const token = getState().auth.token;
    const response = await fetch(
      `https://react-sample-9a61e.firebaseio.com/about/${productId}.json?auth=${token}`,
      {
        method: "DELETE",
      }
    );
    console.log(response);
    // return false;

    if (!response.ok) {
      throw new Error("Something went wrong!");
    }
    dispatch({ type: DELETE_ABOUT, pid: productId });
  };
};

export const createAbout = (title, description, imageUrl, price) => {
  return async (dispatch, getState) => {
    // any async code you want!
    const token = getState().auth.token;
    const userId = getState().auth.userId;
    const response = await fetch(
      `https://react-sample-9a61e.firebaseio.com/about.json?auth=${token}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          description,
          imageUrl,
          price,
          ownerId: userId,
        }),
      }
    );

    const resData = await response.json();

    dispatch({
      type: CREATE_ABOUT,
      productData: {
        id: resData.name,
        title,
        description,
        imageUrl,
        price,
        ownerId: userId,
      },
    });
  };
};

export const updateAbout = (id, title, description, imageUrl) => {
  return async (dispatch, getState) => {
    const token = getState().auth.token;
    const response = await fetch(
      `https://react-sample-9a61e.firebaseio.com/about/${id}.json?auth=${token}`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          description,
          imageUrl,
        }),
      }
    );

    if (!response.ok) {
      throw new Error("Something went wrong!");
    }

    dispatch({
      type: UPDATE_ABOUT,
      pid: id,
      productData: {
        title,
        description,
        imageUrl,
      },
    });
  };
};
